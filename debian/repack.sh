#!/bin/bash
# Repackage MRIcroGL upstream sources. Determine version, strip unnecessary
# files, zip->tar.gz
#
# Usage:
#   repack.sh <source.zip>
#

set -e

ORIGSRC=$1
if [ -z "$ORIGSRC" ]; then
	echo "No upstream sources given."
	exit 1
fi

CURDIR=$(pwd)
WDIR=$(mktemp -d)
SUBDIR=raycast

# put upstream sources into working dir
ORIGSRC_PATH=$(readlink -f ${ORIGSRC})
cd $WDIR
unzip $ORIGSRC_PATH

UPSTREAM_VERSION=$(date --date "$(grep 'str.*MRIcroGL' \
					< $SUBDIR/mainunit.pas \
					| cut -d "'" -f 4,4)" +0.%Y%m%d.1)
ORIG_VERSION="$UPSTREAM_VERSION~dfsg.1"

echo "Determined version: $UPSTREAM_VERSION"
echo "Debian orig version: $ORIG_VERSION"

# cleanup
# leftovers from previous compile runs
find $SUBDIR -name '*.o' -delete -o -name '*.ppu' -delete
find $SUBDIR -name '*.compiled' -delete
find $SUBDIR -name '*.exe' -delete
find $SUBDIR -name '*.or' -delete
find $SUBDIR -name '*.pdf' -delete
find $SUBDIR -name '*.zip' -delete
# no delphi on Debian
rm -rf $SUBDIR/delphionly
# unnecessary pieces
rm -rf $SUBDIR/unused*
rm -rf $SUBDIR/pngimg
rm -rf $SUBDIR/MRIcroGL.app
# other unnecessary pieces
find $SUBDIR -name '*~' -delete
find $SUBDIR -name '*.bak' -delete
find $SUBDIR -iname "Thumbs.db" -delete
find $SUBDIR -iname .DS_Store  -delete
find $SUBDIR -type d -iname backup -delete
rm -f $SUBDIR/simplelaz.res
# liberate documentation
lowriter --convert-to odt manual.doc
lowriter --nologo --headless --convert-to odt --outdir $SUBDIR/manual/ $SUBDIR/manual/manual.doc
rm $SUBDIR/manual/manual.doc

mv $SUBDIR mricrogl-$ORIG_VERSION.orig
tar czf mricrogl_$ORIG_VERSION.orig.tar.gz mricrogl-$ORIG_VERSION.orig
mv mricrogl_$ORIG_VERSION.orig.tar.gz $CURDIR

# clean working dir
rm -rf $WDIR

echo "Tarball is at: $CURDIR/mricrogl_$ORIG_VERSION.orig.tar.gz"

